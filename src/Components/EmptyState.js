import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Result } from 'antd';
import { SmallDashOutlined } from '@ant-design/icons';


class EmptyState extends Component {
    render() {
        return (
            <Result
                status="404"
                className="person-card"
                icon={<SmallDashOutlined />}
                title="You haven't added any related persons"
            />
        );
    }
}
export default connect()(EmptyState);