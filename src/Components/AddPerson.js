import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Drawer, Form, Button, Col, Row, Input, Select, DatePicker, Radio } from 'antd';
import { PlusCircleFilled, ArrowLeftOutlined } from '@ant-design/icons';

import _ from 'lodash';
import '../styles/modal.scss'

const { Option } = Select;


class DrawerForm extends Component {
  state = { 
    visible: false, 
    update: this.props.post ? 1 : 0,
    submit: {
      relationship: this.props.post ? this.props.post.relationship : '', 
      firstname: this.props.post ? this.props.post.firstname : '', 
      lastname: this.props.post ? this.props.post.lastname : '', 
      nationality: this.props.post ? this.props.post.nationality : '', 
      dob: this.props.post ? this.props.post.dob : '', 
      sex: this.props.post ? this.props.post.sex : '',
      resident: this.props.post ? this.props.post.resident : 0
    } 
  };

  showDrawer = () => {
    this.setState({
      ...this.state,
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
      submit: {
        relationship: '', 
        firstname: '', 
        lastname: '', 
        nationality: '', 
        dob: '', 
        sex: '',
        resident: 0
      } 
    });
  };

  onChange = e => {
    this.setState({
      submit: {resident: e.target.value},
    });
  };

  onBlur = e => {
    console.log(e.target.value);
  }

  handleSubmit = (e) => {

    e.preventDefault();
    const data = {
        id: new Date(),
        relationship: this.getRelation.props.value,
        firstname: this.getFirstname.props.value,
        lastname: this.getLastname.props.value,
        sex: this.getSex.props.value,
        nationality: this.getNationality.props.value,
        dob: this.getDate.props.value,
        resident: this.state.submit.resident,
        editing: false
    }

    switch(this.state.update){
      case 1:
        _.merge(data, { id: this.props.post.id})
        this.props.dispatch({
            type: 'UPDATE_RELATED_PERSON',
            data
        })
        break;
      default:
        this.props.dispatch({
            type: 'ADD_RELATED_PERSON',
            data
        })
        break;
    }
    this.onClose()
  }
  
  render() {
    const { update, submit } = this.state;
    return (
      <>
        {!update ? 
        <Button type="primary" onClick={this.showDrawer} className="btn-green">
          <PlusCircleFilled /> {"Add Person"}
        </Button>
        :
        <Button onClick={this.showDrawer}>{"Edit"}</Button>}

        <Drawer
          title={!update ? "Add a related person" : "Edit a related person"}
          width={400}
          onClose={this.onClose}
          visible={this.state.visible}
          bodyStyle={{ paddingBottom: 80 }}
          footer={
            <>
              <Button onClick={this.onClose} className="pull-left">
                <ArrowLeftOutlined />Cancel
              </Button>
              <Button onClick={this.handleSubmit} type="primary" className="btn-green pull-right">
                {update? "Update" : "Add related person"}
              </Button>
            </>
          }
        >
          <Form initialValues={ submit } layout="vertical" hideRequiredMark>
            <Row gutter={16}>
              <Col span={24}>
                <Form.Item
                  name="relationship"
                  label="Relationship"
                  rules={[{ required: true, message: 'Please enter relationship' }]}>
                  <Input 
                    ref={(input) => this.getRelation = input} 
                    placeholder="Please enter relationship" />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item
                  name="firstname"
                  label="First name"
                  rules={[{ required: true, message: 'Please enter first name' }]}>
                  <Input
                    ref={(input) => this.getFirstname = input} 
                    placeholder="Please enter first name" />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item
                  name="lastname"
                  label="Last name"
                  rules={[{ required: true, message: 'Please enter last name' }]}>
                  <Input
                    ref={(input) => this.getLastname = input} 
                    placeholder="Please enter last name" />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item
                  name="sex"
                  label="Sex"
                  rules={[{ required: true, message: 'Please select sex' }]}>
                  <Select placeholder="Please select sex"
                    ref={(input) => this.getSex = input}>
                    <Option value="m">Male</Option>
                    <Option value="f">Female</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item
                  name="dob"
                  label="Date of birth"
                  rules={[{ required: true, message: 'Please choose the date of birth' }]}
                >
                  <DatePicker
                    style={{ width: '100%' }}
                    ref={(input) => this.getDate = input}
                    getPopupContainer={trigger => trigger.parentElement}
                  />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item
                  name="nationality"
                  label="Nationality"
                  rules={[{ required: true, message: 'Please enter nationality' }]}
                >
                  <Input value={this.state.submit.nationality} 
                    ref={(input) => this.getNationality = input} 
                    placeholder="Please enter nationality" />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item
                  name="resident"
                  label="Permanent South African resident?"
                  rules={[{ required: true, message: 'Please select residency status' }]}
                >
                  <Radio.Group 
                    onChange={this.onChange} 
                    onFocus={this.onChange} 
                    name="resident">
                    <Radio value={0}>No</Radio>
                    <Radio value={1}>Yes</Radio>
                  </Radio.Group>
                </Form.Item>
              </Col>
            </Row>
            {/* <Row gutter={16}>
              <Col span={24}>
                <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                  Cancel
                </Button>
                <Button onClick={this.handleSubmit} type="primary">
                  {update? "Update" : "Add related person"}
                </Button>
              </Col>
            </Row> */}
          </Form>
        </Drawer>
      </>
    );
  }
}
export default connect()(DrawerForm);