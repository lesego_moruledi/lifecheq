import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Menu, Dropdown, Button, Modal } from 'antd';
import { 
    DownOutlined, 
    ExclamationCircleOutlined 
} from '@ant-design/icons';
import { DeleteForever } from '@material-ui/icons';

const { confirm } = Modal

class DeleteComponent extends Component {
    showPromiseConfirm = (e) => {
        const title = `Remove ${e.post.firstname} ${e.post.lastname}` 
        return (
            confirm({
                title,
                icon: <ExclamationCircleOutlined />,
                content: 'This beneficiary will not receive any benefits.',
                okText:'Remove beneficiary',
                okType: 'danger',
                onOk() {
                    e.dispatch({
                        id: e.post.id,
                        type: 'DELETE_RELATED_PERSON'
                    })
                },
                onCancel() {},
            })
        );
    }

   menu = () => {
       return (
        <Menu>
            <Menu.Item className="menu-item" onClick={(e)=> this.showPromiseConfirm(this.props)}>
                <DeleteForever className="icon"/> Remove
            </Menu.Item>
        </Menu>
        );
    }
  
  render() {
    return (
      <>
        <Dropdown overlay={this.menu} trigger={['click']}>
            <Button>
                <DownOutlined />
            </Button>
        </Dropdown>
        
      </>
    );
  }
}
export default connect()(DeleteComponent);