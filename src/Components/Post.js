import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Card } from 'antd';
import AddPerson from './AddPerson';
import DeleteComponent from './DeleteComponent';


class Post extends Component {
    render() {
        return (
            <Card className="person-card">
                <h2>{this.props.post.firstname} {this.props.post.lastname}</h2>
                <p>{this.props.post.relationship}</p>
                <div className="control-buttons">
                    <AddPerson post={this.props.post} />
                    <DeleteComponent post={this.props.post }/>
                </div>
            </Card>
        );
    }
}
export default connect()(Post);