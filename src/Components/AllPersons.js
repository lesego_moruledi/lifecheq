import React, { Component } from 'react';
import { connect } from 'react-redux';
import Post from './Post';
import EmptyState from './EmptyState';

class AllPost extends Component {
    render() {
        return (
            <div>
                <div className="post-grid">
                {this.props.posts.length>0 && this.props.posts.map((post) => <Post post={post} key={post.id}
                     />)}
                </div>
                {this.props.posts.length<1 && <EmptyState />}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state
    }
}
export default connect(mapStateToProps)(AllPost);