import AllPersons from './Components/AllPersons';
import AddPerson from './Components/AddPerson';

function App() {
  return (
    <div className="App">
      <div>
        <h2>Related Persons</h2>
      </div>
      <AllPersons />
      <AddPerson />
    </div>
  );
}

export default App;
