import _ from 'lodash'


const postReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_RELATED_PERSON':
            return state.concat([action.data])
        case 'DELETE_RELATED_PERSON':
            return state.filter((post) => post.id !== action.id)
        case 'EDIT_RELATED_PERSON':
            return state.map((post) => post.id === action.id ? { ...post, editing: !post.editing } : post)
        case 'UPDATE_RELATED_PERSON':
            return state.map((post) => {
                if (post.id === action.data.id) {
                    _.merge(post, action.data)
                    return {
                        ...post,
                        editing: !post.editing
                    }
                } else return post;
            })
        default:
            return state;
    }
}
export default postReducer;